﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace BubblePop
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class BubblePlaceholder : MonoBehaviour
    {
        [SerializeField]
        private float _opacity;

        private SpriteRenderer _renderer;

        private void Awake()
        {
            _renderer = GetComponent<SpriteRenderer>();
        }

        public Color color
        {
            set
            {
                Color color = value;
                color.a = _opacity;
                _renderer.color = color;
            }
        }

        public void SetActive(bool value)
        {
                gameObject.SetActive(value);
        }
    }
}