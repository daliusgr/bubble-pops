﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

namespace BubblePop
{
    [RequireComponent(typeof(Grid))]
    public class GridView : MonoBehaviour
    {
        public BubbleSpawner spawner;
        public BubblePlaceholder bubblePlaceholder;
        public ScoreDisplay scoreDisplay;
        public ParticleSystem burstEffect;

        public float waitTime = 0.2F;

        public Vector2 startPosition;
        public Launcher launcher;

        public bool isDebugEnabled;

        private Grid _grid;

        private Bubble[,] _bubbles;
        private float _bubbleRadius = -1;
        private int _lastAimX;
        private int _lastAimY;

        private float _heightOffset = Mathf.Sqrt(3);

        private void Awake()
        {
            _grid = GetComponent<Grid>();
            _bubbles = new Bubble[_grid.maxHeight, _grid.width];
            bubblePlaceholder.SetActive(false);
        }

        void Start()
        {
            AssignRadiusAndHeightOffset();

            for (int y = 0; y < _grid.maxHeight; y++)
            {
                for (int x = 0; x < _grid.width; x++)
                {
                    _SpawnBubbleCheked(x, y);
                }
            }

            launcher.OnAim += (hit, direction, color) =>
            {
                Vector3 point = hit.point;
                bubblePlaceholder.SetActive(true);
                bubblePlaceholder.color = color;

                _lastAimY = Mathf.CeilToInt(Mathf.Clamp(startPosition.y - point.y, 0, _grid.maxHeight-1) / _heightOffset);

                float xOffset = _GetXOffset(_lastAimY);
                _lastAimX = _GetClosestX(point, hit.normal, xOffset);


                // Corner case where hit normal points to an occupied location
                if (_grid.IsOccupied(_lastAimX, _lastAimY))
                {
                    _lastAimX = _GetClosestX(point, direction, xOffset);
                }

                // Corner case where height is estimated correctly but space is occupied
                if (_grid.IsOccupied(_lastAimX, _lastAimY))
                {
                    _lastAimY--;
                    xOffset = _GetXOffset(_lastAimY);
                    _lastAimX = _GetClosestX(point, hit.normal, xOffset);
                }

                bubblePlaceholder.transform.position = _GridToViewPoint(_lastAimX, _lastAimY, _bubbleRadius);
            };

            launcher.OnNoAim += () =>
            {
                bubblePlaceholder.SetActive(false);
            };

            launcher.OnLaunch += (points, count, number) =>
            {
                launcher.SetPaused(true);
                bubblePlaceholder.SetActive(false);
                List<Action> actions = _grid.SetCell(_lastAimX, _lastAimY, number);
                StartCoroutine(_ProcessActions(actions, points, count));
            };
        }

        private void MyDestroy(Bubble bubble)
        {
            var effect = Instantiate(burstEffect, bubble.transform.position, Quaternion.identity);
            var main = effect.main;
            main.startColor = bubble.color;
            effect.Play();
            StartCoroutine(DestroyEffect(effect));
            Destroy(bubble.gameObject);
        }

        private IEnumerator DestroyEffect(ParticleSystem effect)
        {
            yield return new WaitForSeconds(effect.main.duration);
            Destroy(effect.gameObject);
        }

        private void AssignRadiusAndHeightOffset()
        {
            Bubble bubble = spawner.Spawn(2);
            _bubbleRadius = bubble.radius;
            _heightOffset *= _bubbleRadius;
            Destroy(bubble.gameObject);
        }

        private IEnumerator _ProcessActions(List<Action> actions, Vector3[] points, int pointCount)
        {
            Sequence seq;

            yield return LaunchBubble(actions, points, pointCount).WaitForCompletion();

            // First action is always launching, so start from second
            for (int actionIndex = 1; actionIndex < actions.Count; actionIndex++)
            {
                Action a = actions[actionIndex];
                switch (a.type)
                {
                    case ActionType.ChangeNumber:
                        Bubble b = _bubbles[a.points[0].y, a.points[0].x];
                        Log("Change bubble number to " + a.value);
                        spawner.ChangeBubbleNumber(b, a.value);
                        scoreDisplay.AddToScore(a.value);
                        break;
                    case ActionType.Move:
                        Bubble end = _bubbles[a.end.y, a.end.x];

                        Log("Move " + a.points.Length + " bubbles");

                        seq = DOTween.Sequence();

                        for (int i = 0; i < a.points.Length; i++)
                        {
                            Bubble bubble = _bubbles[a.points[i].y, a.points[i].x];
                            if (bubble != end)
                            {
                                int index = i;
                                var tween = bubble.transform
                                    .DOMove(end.transform.position, waitTime)
                                    .OnComplete(() =>
                                    {
                                        MyDestroy(bubble);
                                        _bubbles[a.points[index].y, a.points[index].x] = null;
                                    });
                                seq.Join(tween);
                            }
                        }

                        yield return seq.WaitForCompletion();
                        break;
                    case ActionType.Drop:
                        Log("Drop the base");
                        seq = DOTween.Sequence();

                        for (int i = 0; i < a.points.Length; i++)
                        {
                            Bubble bubble = _bubbles[a.points[i].y, a.points[i].x];
                            Vector3 endOPos = launcher.transform.position;
                            endOPos.x = bubble.transform.position.x;
                            int index = i;
                            var tween = bubble.transform
                                .DOMove(endOPos, waitTime)
                                .OnComplete(() =>
                                {
                                    MyDestroy(bubble);
                                    _bubbles[a.points[index].y, a.points[index].x] = null;
                                });
                            seq.Join(tween);
                        }

                        yield return seq.WaitForCompletion();
                        break;
                    case ActionType.Explosion:
                        Log("Explode");
                        for (int i = 0; i < a.points.Length; i++)
                        {
                            MyDestroy(_bubbles[a.points[i].y, a.points[i].x]);
                            _bubbles[a.points[i].y, a.points[i].x] = null;
                        }
                        scoreDisplay.AddToScore(a.value);
                        break;
                    default:
                        break;
                }
            }

            int adjust = _grid.GetBoardHeightAdjust();
            if(adjust != 0)
            {
                seq = DOTween.Sequence();
                if(adjust == 1)
                {
                    MoveBubblesDown(seq);
                } else
                {
                    MoveBubblesUp(seq);
                }
                yield return seq.WaitForCompletion();
            }

            Log("Done processing actions");
            CheckBubbles();
            launcher.SetPaused(false);
        }

        private void MoveBubblesDown(Sequence seq)
        {
            for (int y = _grid.maxHeight - 1; y > 0; y--)
            {
                for (int x = 0; x < _grid.width; x++)
                {
                    if (_bubbles[y - 1, x])
                    {
                        _bubbles[y, x] = _bubbles[y - 1, x];
                        _bubbles[y - 1, x] = null;
                        var trans = _bubbles[y, x].transform;
                        var tween = trans.DOMoveY(trans.position.y - _heightOffset, waitTime);
                        seq.Join(tween);
                    }
                }
            }

            for (int x = 0; x < _grid.width; x++)
            {
                _SpawnBubbleCheked(x, 0);
            }
        }

        private void MoveBubblesUp(Sequence seq)
        {
            for (int x = 0; x < _grid.width; x++)
            {
                if (_bubbles[0, x])
                {
                    Destroy(_bubbles[0, x].gameObject);
                    _bubbles[0, x] = null;
                }
            }

            for (int y = 0; y < _grid.maxHeight - 1; y++)
            {
                for (int x = 0; x < _grid.width; x++)
                {
                    if (_bubbles[y + 1, x])
                    {
                        _bubbles[y, x] = _bubbles[y + 1, x];
                        _bubbles[y + 1, x] = null;
                        var trans = _bubbles[y, x].transform;
                        var tween = trans.DOMoveY(trans.position.y + _heightOffset, waitTime);
                        seq.Join(tween);
                    }
                }
            }
        }

        private Tweener LaunchBubble(List<Action> actions, Vector3[] points, int pointCount)
        {
            // Create the launch bubble at the launcher position
            Action start = actions[0];
            Bubble newBubble = spawner.Spawn(start.value);
            _bubbles[start.points[0].y, start.points[0].x] = newBubble;
            newBubble.transform.parent = transform;
            newBubble.transform.position = launcher.transform.position;
            spawner.ChangeBubbleNumber(newBubble, start.value);

            // Define a path for the bubble
            Vector3[] path = new Vector3[pointCount];
            for (int i = 0; i < pointCount; i++)
            {
                path[i] = points[i];
            }
            path[pointCount - 1] = _GridToViewPoint(start.points[0].x, start.points[0].y, _bubbleRadius);

            // Launch the bubble and return the tween
            return newBubble.transform.DOPath(path, waitTime).SetEase(Ease.Linear);
        }

        private void _SpawnBubbleCheked(int x, int y)
        {
            int num = _grid.CellAt(x, y);
            if (num > 0)
            {
                _SpawnBubble(x, y, num);
            }
        }

        private void _SpawnBubble(int x, int y, int num)
        {
            Bubble bubble = spawner.Spawn(num);
            bubble.xGrid = x;
            bubble.yGrid = y;
            bubble.transform.position = _GridToViewPoint(x, y, _bubbleRadius);
            bubble.transform.SetParent(transform);

            _bubbles[y, x] = bubble;
        }

        private void CheckBubbles()
        {
            if (isDebugEnabled)
            {
                for (int y = 0; y < _grid.maxHeight; y++)
                {
                    for (int x = 0; x < _grid.width; x++)
                    {
                        int num = _grid.CellAt(x, y);

                        if (_bubbles[y, x] == null && _grid.IsOccupied(x, y))
                        {
                            Log("Mismatch: bubble not created");
                        }
                        else if (_bubbles[y, x] != null && num != _bubbles[y, x].number)
                        {
                            if (_grid.IsOccupied(x, y))
                            {
                                Log("Mismatch: bubble wrong number");
                            }
                            else
                            {
                                Log("Mismatch: bubble not destroyed");
                            }
                        }
                    }
                }
            }
        }

        private int _GetClosestX(Vector3 point, Vector3 normal, float xOffset)
        {
            return Mathf.RoundToInt(Mathf.Abs((point.x + normal.x * _bubbleRadius) - (startPosition.x + xOffset)) / (_bubbleRadius * 2));
        }

        private float _GetXOffset(int y)
        {
            return ((y + _grid.totalAdjust) % 2 == 0 ? 0 : -1) * _bubbleRadius;
        }

        private Vector3 _GridToViewPoint(int x, int y, float radius)
        {
            float xCoord = startPosition.x + radius * 2 * x + _GetXOffset(y);
            float yCoord = startPosition.y - _heightOffset * y;
            return new Vector3(xCoord, yCoord, 0);
        }

        private void Log(string log)
        {
            if (isDebugEnabled)
            {
                Debug.Log(log);
            }
        }
    }
}