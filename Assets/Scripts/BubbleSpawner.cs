﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubblePop {
    public class BubbleSpawner : MonoBehaviour
    {
        [SerializeField]
        private Bubble _bubbleProto;

        [SerializeField]
        private ColorMap _numToColorMap;

        private Dictionary<int, Color> _colorMap;

        private void Awake()
        {
            _colorMap = _numToColorMap.GetMap();
        }

        public Bubble Spawn(int number)
        {
            Bubble bubble = Instantiate(_bubbleProto);
            ChangeBubbleNumber(bubble, number);
            return bubble;
        }

        public void ChangeBubbleNumber(Bubble bubble, int newNumber)
        {
            if (_colorMap.ContainsKey(newNumber))
            {
                bubble.color = _colorMap[newNumber];
            }
            bubble.number = newNumber;
        }
    }
}