﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace BubblePop
{
    public class ScoreDisplay : MonoBehaviour
    {
        public TextMeshPro text;

        private const string SCORE_KEY = "Score";
        private int _score;

        private void Awake()
        {
            if (PlayerPrefs.HasKey(SCORE_KEY))
            {
                _score = PlayerPrefs.GetInt(SCORE_KEY);
            } else
            {
                _score = 0;
            }
            UpdateScoreText();
        }

        private void UpdateScoreText()
        {
            text.text = _score.ToString();
        }

        public void AddToScore(int adition)
        {
            _score += adition;
            UpdateScoreText();
            PlayerPrefs.SetInt(SCORE_KEY, _score);
        }
    }
}