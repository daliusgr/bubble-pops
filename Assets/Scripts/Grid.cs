﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

namespace BubblePop
{
    public class Grid : MonoBehaviour
    {
        public int width;
        public int startHeight;
        public int maxHeight;
        public int minCount;

        public bool shouldStartNew;

        [SerializeField]
        private BubbleServer _server;

        private const string GRID_KEY = "Grid";
        private int[,] _grid;

        private Point[][] _neighbours = new Point[2][]
        {
            new Point[]
            {
                new Point(-1, 0), new Point(1, 0), new Point(0, 1), new Point(0, -1),
                new Point(1, 1), new Point(1, -1)
            },
            new Point[]
            {
                new Point(-1, 0), new Point(1, 0), new Point(0, 1), new Point(0, -1),
                new Point(-1, 1), new Point(-1, -1)
            }
        };
        public int totalAdjust = 0;

        private void Awake()
        {
            if (PlayerPrefs.HasKey(GRID_KEY) && !shouldStartNew)
            {
                string json = PlayerPrefs.GetString(GRID_KEY);
                _grid = JsonConvert.DeserializeObject<int[,]>(json);
            }
            else
            {
                _grid = new int[maxHeight, width];
                for (int y = 0; y < maxHeight; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        if (y < startHeight)
                        {
                            _AssignRandom(y, x);
                        }
                        else
                        {
                            _grid[y, x] = 0;
                        }
                    }
                }
            }
        }

        private void OnApplicationQuit()
        {
            SaveGrid();
        }

        private void OnApplicationFocus(bool focus)
        {
            SaveGrid();
        }

        private void OnApplicationPause(bool pause)
        {
            SaveGrid();
        }

        private void SaveGrid()
        {
            string json = JsonConvert.SerializeObject(_grid);
            PlayerPrefs.SetString(GRID_KEY, json);
        }

        private void _AssignRandom(int y, int x)
        {
            _grid[y, x] = _server.GetNext();
        }

        public int CellAt(int x, int y)
        {
            if (_IsInBounds(x, y))
            {
                return _grid[y, x];
            }
            else
            {
                return -1;
            }
        }

        private bool _IsInBounds(int x, int y)
        {
            return x >= 0 && x < width && y >= 0 && y < maxHeight;
        }

        public bool IsOccupied(int x, int y)
        {
            return CellAt(x, y) != 0;
        }

        /// <summary>
        /// Check if the board needs adjusting to fit all bubbles
        /// or to extend
        /// </summary>
        /// <returns>0: no adjustment, 1: added a new row, -1: removed a row</returns>
        public int GetBoardHeightAdjust()
        {
            bool isCountTooLow = CountBubbles() < minCount;
            bool isRowBeforeLastOccupied = IsRowOccupied(maxHeight-2);
            bool isLastRowOccupied = IsRowOccupied(maxHeight-1);

            if(isCountTooLow && !isRowBeforeLastOccupied)
            {
                ShiftDown();
                totalAdjust = totalAdjust == 0 ? 1 : 0;
                return 1;
            }
            else if (isLastRowOccupied)
            {
                ShiftUp();
                totalAdjust = totalAdjust == 0 ? 1 : 0;
                return -1;
            }

            return 0;
        }

        private int CountBubbles()
        {
            int count = 0;
            for (int y = 0; y < maxHeight; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (IsOccupied(x, y))
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        // Shifts grid up
        private void ShiftUp()
        {
            for (int y = 0; y < maxHeight-1; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    _grid[y, x] = _grid[y+1, x];
                }
            }

            for (int x = 0; x < width; x++)
            {
                _grid[maxHeight - 1, x] = 0;
            }
        }

        // Shifts grid down, populating the first row
        private void ShiftDown()
        {
            for (int y = maxHeight - 1; y > 0; y--)
            {
                for (int x = 0; x < width; x++)
                {
                    _grid[y, x] = _grid[y-1, x];
                }
            }

            for (int x = 0; x < width; x++)
            {
                _grid[0, x] = _server.GetNext();
            }
        }

        private bool IsRowOccupied(int y)
        {
            for (int x = 0; x < width; x++)
            {
                if (IsOccupied(x, y))
                {
                    return true;
                }
            }
            return false;
        }

        public List<Action> SetCell(int x, int y, int value)
        {
            List<Action> actionSequence = new List<Action>();
            Action action;

            // First, action of setting this cell to a value
            AddSetAction(x, y, value, actionSequence);

            CheckForExplosion(x, y, value, actionSequence);

            // Then check for combinations
            List<Point> ns = GetNeighboursWithValueRecursive(x, y, value, new HashSet<Point>());
            if (ns.Count > 0)
            {
                // Neighbours with same value are found, there will be a combination so zero everything
                for (int i = 0; i < ns.Count; i++)
                {
                    _grid[ns[i].y, ns[i].x] = 0;
                }
                _grid[y, x] = 0;

                int combinedValue = value * (1 << (ns.Count - 1));

                int endIndex = -1;
                ns.Add(new Point(x, y));

                // Check if any neighbour has the combinedValue
                // to favour that neighbour direction when merging bubbles
                for (int i = 0; i < ns.Count; i++)
                {
                    List<Point> same = GetNeighboursWithValue(ns[i].x, ns[i].y, combinedValue);
                    if (same.Count > 0)
                    {
                        endIndex = i;
                        break;
                    }
                }

                // Define end point
                endIndex = endIndex == -1 ? 0 : endIndex;
                Point end = ns[endIndex];
                ns.RemoveAt(endIndex);

                action = new Action()
                {
                    points = ns.ToArray(),
                    end = end,
                    value = combinedValue,
                    type = ActionType.Move
                };
                actionSequence.Add(action);

                // Recursive add
                actionSequence.AddRange(SetCell(end.x, end.y, combinedValue));
            }

            // Check if there are detached bubbles
            CheckForDetached(actionSequence);

            return actionSequence;
        }

        // Explosion if more than 2K
        private void CheckForExplosion(int x, int y, int value, List<Action> actionSequence)
        {
            if (value > 2000)
            {
                List<Point> toExplode = GetNeighboursInLongRange(x, y);
                toExplode.Add(new Point(x, y));

                int totalScore = 0;
                for (int i = 0; i < toExplode.Count; i++)
                {
                    Point p = toExplode[i];
                    totalScore += CellAt(p.x, p.y);
                    _grid[p.y, p.x] = 0;
                }

                var action = new Action()
                {
                    points = toExplode.ToArray(),
                    value = totalScore,
                    type = ActionType.Explosion
                };
                actionSequence.Add(action);
            }
        }

        private void AddSetAction(int x, int y, int value, List<Action> actionSequence)
        {
            Action action = new Action()
            {
                points = new Point[1] { new Point(x, y) },
                value = value,
                type = ActionType.ChangeNumber
            };
            actionSequence.Add(action);
            _grid[y, x] = value;
        }

        private void CheckForDetached(List<Action> actionSequence)
        {
            List<Point> detached = GetDetachedBubbles();
            if (detached.Count > 0)
            {
                for (int i = 0; i < detached.Count; i++)
                {
                    Point p = detached[i];
                    _grid[p.y, p.x] = 0;
                }

                Action action = new Action()
                {
                    points = detached.ToArray(),
                    type = ActionType.Drop
                };
                actionSequence.Add(action);
            }
        }

        private List<Point> GetDetachedBubbles()
        {
            HashSet<Point> visited = new HashSet<Point>();
            Queue<Point> next = new Queue<Point>();

            // Add the top row to visited list,
            // and neighbours to the check queue
            for(int x = 0; x < width; x++)
            {
                if(IsOccupied(x, 0))
                {
                    visited.Add(new Point(x, 0));
                    var ns = GetNeighbourCoords(x, 0);

                    for(int i = 0; i < ns.Count; i++)
                    {
                        if (!visited.Contains(ns[i]))
                        {
                            next.Enqueue(ns[i]);
                        }
                    }
                }
            }

            // Do BFS to find all connected bubbles
            while(next.Count > 0)
            {
                Point p = next.Dequeue();
                visited.Add(p);
                var ns = GetNeighbourCoords(p.x, p.y);
                for (int i = 0; i < ns.Count; i++)
                {
                    if (!visited.Contains(ns[i]))
                    {
                        next.Enqueue(ns[i]);
                    }
                }
            }

            // Bubbles that were not visited are not connected
            List<Point> result = new List<Point>();
            for(int y = 0; y < maxHeight; y++)
            {
                for(int x = 0; x < width; x++)
                {
                    var p = new Point(x, y);
                    if (!visited.Contains(p) && IsOccupied(x, y))
                    {
                        result.Add(p);
                    }
                }
            }

            return result;
        }

        public List<Point> GetNeighboursInLongRange(int x, int y)
        {
            List<Point> result = new List<Point>();
            HashSet<Point> visited = new HashSet<Point>();

            visited.Add(new Point(x, y));

            AddNeigboursToResult(result, visited, GetNeighbourCoords(x, y));

            List<Point> furtherNs = new List<Point>();
            for (int i = 0; i < result.Count; i++)
            {
                AddNeigboursToResult(furtherNs, visited, GetNeighbourCoords(result[i].x, result[i].y));
            }
            result.AddRange(furtherNs);
            return result;
        }

        private void AddNeigboursToResult(List<Point> result, HashSet<Point> visited, List<Point> ns)
        {
            for (int i = 0; i < ns.Count; i++)
            {
                if (!visited.Contains(ns[i]))
                {
                    result.Add(ns[i]);
                    visited.Add(ns[i]);
                }
            }
        }

        public List<Point> GetNeighbourCoords (int x, int y)
        {
            List<Point> result = new List<Point>();

            Point[] ns = GetNeighbourOffsets(y);

            for (int i = 0; i < ns.Length; i++)
            {
                if(_IsInBounds(x + ns[i].x, y + ns[i].y) && IsOccupied(x + ns[i].x, y + ns[i].y))
                {
                    result.Add(new Point(x + ns[i].x, y + ns[i].y));
                }
            }

            return result;
        }

        private Point[] GetNeighbourOffsets(int y)
        {
            return _neighbours[(y + totalAdjust) % 2];
        }

        private List<Point> GetNeighboursWithValue(int x, int y, int val)
        {
            List<Point> result = new List<Point>();

            Point[] ns = GetNeighbourOffsets(y);

            for (int i = 0; i < ns.Length; i++)
            {
                if (CellAt(x + ns[i].x, y + ns[i].y) == val)
                {
                    result.Add(new Point(x + ns[i].x, y + ns[i].y));
                }
            }

            return result;
        }

        private List<Point> GetNeighboursWithValueRecursive(int x, int y, int val, HashSet<Point> visited)
        {
            List<Point> result = new List<Point>();
            List<Point> ns = GetNeighboursWithValue(x, y, val);

            for(int i = 0; i < ns.Count; i++)
            {
                if (!visited.Contains(ns[i]))
                {
                    result.Add(ns[i]);
                    visited.Add(ns[i]);
                }
            }

            for (int i = 0; i < result.Count; i++)
            {
                Point p = result[i];
                result.AddRange(GetNeighboursWithValueRecursive(p.x, p.y, val, visited));
            }

            return result;
        }
    }

    public struct Point: System.IEquatable<Point>
    {
        public readonly int x;
        public readonly int y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public bool Equals(Point other)
        {
            return this.x == other.x && this.y == other.y;
        }
    }

    public enum ActionType
    {
        ChangeNumber,
        Move,
        Drop,
        Explosion
    }

    public struct Action
    {
        public Point[] points;
        public Point end;
        public int value;
        public ActionType type;
    }
}