﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace BubblePop
{
    [RequireComponent(typeof(CircleCollider2D), typeof(SpriteRenderer))]
    public class Bubble : MonoBehaviour
    {
        public TextMeshPro text;

        [HideInInspector]
        public int xGrid;
        [HideInInspector]
        public int yGrid;

        private CircleCollider2D _collider;
        private SpriteRenderer _render;
        private int _number;

        private void Awake()
        {
            _collider = GetComponent<CircleCollider2D>();
            _render = GetComponent<SpriteRenderer>();
        }

        public float radius
        {
            get { return _collider.radius; }
        }

        public void DisableCollider()
        {
            _collider.enabled = false;
        }

        public int number
        {
            set
            {
                _number = value;
                if (number > 1000)
                {
                    text.text = string.Format("{0}K", value / 1000);
                }
                else
                {
                    text.text = value.ToString();
                }
            }

            get
            {
                return _number;
            }
        }

        public Color color
        {
            set { _render.color = value; }
            get { return _render.color; }
        }
    }
}