﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubblePop
{
    public delegate void LaunchEventHandler(Vector3[] points, int count, int bubbleNumber);
    public delegate void AimEventHandler(RaycastHit2D hit, Vector3 direction, Color color);
    public delegate void NoAimEventHandler();

    public class Launcher : MonoBehaviour
    {
        [SerializeField]
        private LineRenderer _line;

        [SerializeField]
        private BubbleServer _server;

        [SerializeField]
        private BubbleSpawner _spawner;

        public float cameraDistance;

        public event LaunchEventHandler OnLaunch;
        public event AimEventHandler OnAim;
        public event NoAimEventHandler OnNoAim;

        public string ballTag;
        public string wallTag;

        private Vector3 _startPos;
        private Vector3[] _linePositions;
        private int _positionCount;

        private RaycastHit2D[] _rayHits;
        private Camera _camera;

        private Bubble[] _preloads;
        private bool _isPaused;
        private const int PRELOAD_COUNT = 2;

        private void Awake()
        {
            _camera = Camera.main;
            _startPos = transform.position;
            _linePositions = new Vector3[3];
            for(int i = 0; i < 3; i++)
            {
                _linePositions[i] = Vector3.zero;
            }
            _linePositions[0] = _startPos;
            _rayHits = new RaycastHit2D[1];

            _preloads = new Bubble[PRELOAD_COUNT];
        }

        private void Start()
        {
            for (int i = 0; i < PRELOAD_COUNT; i++)
            {
                Bubble bubble = _spawner.Spawn(_server.GetNext());
                bubble.DisableCollider();
                _preloads[i] = bubble;
            }

            ArrangePreloads();
        }

        public void SetPaused(bool isPaused)
        {
            _isPaused = isPaused;
        }

        private void ArrangePreloads()
        {
            for (int i = PRELOAD_COUNT - 1; i >= 0; i--)
            {
                Bubble next = _preloads[i];
                next.transform.position = transform.position - (new Vector3(next.radius * 2, 0, 0)) * (PRELOAD_COUNT - 1 - i);
            }
        }

        void Update()
        {
            if (!_isPaused)
            {
                if (Input.GetMouseButton(0))
                {
                    Vector3 direction = (_GetWorldMousePos() - _startPos).normalized;
                    _RaycastFrom(_startPos, direction);
                    _positionCount = 2;
                    _linePositions[1] = _rayHits[0].point;

                    if (_isHittingAWall())
                    {
                        Vector3 reflected = Vector3.Reflect(direction, _rayHits[0].normal).normalized;
                        _RaycastFrom(_rayHits[0].point + _rayHits[0].normal * 0.1f, reflected);
                        _linePositions[2] = _rayHits[0].point;
                        _positionCount = 3;

                        if (_isHittingAWall())
                        {
                            _positionCount = 0;
                        }
                    }

                    _line.positionCount = _positionCount;
                    if (_positionCount > 0)
                    {
                        _line.SetPositions(_linePositions);
                        if (OnAim != null)
                        {
                            OnAim(_rayHits[0],
                                (_linePositions[_positionCount - 2] - _linePositions[_positionCount - 1]).normalized,
                                currentBubble.color);
                            Debug.DrawRay(_rayHits[0].point, _rayHits[0].normal);
                        }
                    }
                    else
                    {
                        if (OnNoAim != null)
                        {
                            OnNoAim();
                        }
                    }
                }

                if (Input.GetMouseButtonUp(0))
                {
                    if (_positionCount > 0)
                    {
                        if (OnLaunch != null)
                        {
                            OnLaunch(_linePositions, _positionCount, currentBubble.number);
                            DisposeCurrentPreload();
                            ArrangePreloads();
                        }

                        _line.positionCount = 0;
                    }
                    else
                    {
                        if (OnNoAim != null)
                        {
                            OnNoAim();
                        }
                    }
                }
            }
        }

        private void DisposeCurrentPreload()
        {
            _spawner.ChangeBubbleNumber(currentBubble, _server.GetNext());
            Bubble tmp = _preloads[PRELOAD_COUNT - 1];
            _preloads[PRELOAD_COUNT-1] = _preloads[0];
            _preloads[0] = tmp;
        }

        private Bubble currentBubble
        {
            get { return _preloads[PRELOAD_COUNT - 1]; }
        }

        private bool _isHittingAWall()
        {
            return _rayHits[0].collider.tag.Equals(wallTag);
        }

        private Vector3 _GetWorldMousePos()
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = cameraDistance;
            return _camera.ScreenToWorldPoint(mousePos);
        }

        private void _RaycastFrom(Vector3 position, Vector3 direction)
        {
            Physics2D.RaycastNonAlloc(position, direction, _rayHits);
        }
    }
}