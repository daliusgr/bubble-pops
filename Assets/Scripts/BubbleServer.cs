﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubblePop
{
    public class BubbleServer : MonoBehaviour
    {
        public int maxPowerOf2;

        public int GetNext()
        {
            return 1 << Random.Range(1, maxPowerOf2);
        }
    }
}