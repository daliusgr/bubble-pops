﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubblePop
{
    [CreateAssetMenu(menuName ="BubblePop/ColorMap")]
    public class ColorMap : ScriptableObject
    {
        [SerializeField]
        private NumToColor[] _map;

        private Dictionary<int, Color> _colorMap;

        public Dictionary<int, Color> GetMap()
        {
            if(_colorMap == null)
            {
                _colorMap = new Dictionary<int, Color>();
                foreach(NumToColor pair in _map)
                {
                    _colorMap.Add(pair.key, pair.value);
                }
            }
            return _colorMap;
        }
    }

    [System.Serializable]
    class NumToColor
    {
        public int key;
        public Color value;
    }
}